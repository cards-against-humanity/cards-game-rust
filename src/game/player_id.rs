use crate::proto::{player::Identifier, Player};
use std::hash::Hash;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum PlayerId {
    RealUser(String),
    ArtificialPlayer(String),
}

impl PlayerId {
    pub fn from_player_proto(player: &Player) -> Option<PlayerId> {
        match &player.identifier {
            Some(identifier) => match identifier {
                Identifier::User(user) => Some(PlayerId::RealUser(String::from(&user.name))),
                Identifier::ArtificialUser(artificial_user) => Some(PlayerId::ArtificialPlayer(
                    String::from(&artificial_user.id),
                )),
            },
            None => None,
        }
    }
}

// TODO - Use these new structs.
// use std::cmp::Eq;
// use crate::proto::{User, ArtificialUser, Player};
// use tonic::Status;

// #[derive(Clone, Debug, PartialEq, Eq)]
// pub struct UserName(String);

// const USER_NAME_PARSE_ERROR: Status = Status::invalid_argument("User name must follow the format \"users/{id}\".");

// impl UserName {
//     pub fn new(user_name_string: String) -> Result<Self, Status> {
//         let segments: Vec<&str> = user_name_string.split("/").collect();
//         match segments.first() {
//             Some(first_segment) => {
//                 if *first_segment != "users" {
//                     return Err(USER_NAME_PARSE_ERROR);
//                 }
//             },
//             None => {}
//         };
//         if segments.len() != 2 {
//             return Err(USER_NAME_PARSE_ERROR);
//         }
//         Ok(UserName(user_name_string))
//     }
// }

// #[derive(Clone, Debug, PartialEq, Eq)]
// pub struct ArtificialPlayerId(String);

// #[derive(Clone, Debug, PartialEq, Eq)]
// pub enum PlayerId {
//     RealUser(UserName),
//     ArtificialPlayer(ArtificialPlayerId),
// }

// impl PlayerId {
//     pub fn new_from_player_proto(player: Player) -> Result<Self, Status> {
//         match player.identifier {
//             Some(identifier) => {},
//             None => {}
//         };
//         Self::new(user_name_string: String)
//     }
// }

// impl UserName {
//     pub fn stringify(self) -> String {
//         self.0
//     }
// }

// impl PartialEq<User> for UserName {
//     fn eq(&self, other: &User) -> bool {
//         &self.0 == &other.name
//     }
// }

// impl PartialEq<UserName> for User {
//     fn eq(&self, other: &UserName) -> bool {
//         &self.name == &other.0
//     }
// }

// impl PartialEq<ArtificialUser> for ArtificialPlayerId {
//     fn eq(&self, other: &ArtificialUser) -> bool {
//         &self.0 == &other.id
//     }
// }

// impl PartialEq<ArtificialPlayerId> for ArtificialUser {
//     fn eq(&self, other: &ArtificialPlayerId) -> bool {
//         &self.id == &other.0
//     }
// }
